msgid ""
msgstr ""
"Project-Id-Version: Contact Form\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-07-12 17:21+0300\n"
"PO-Revision-Date: 2016-07-12 17:21+0300\n"
"Last-Translator: plugin@bestwebsoft.com <plugin@bestwebsoft.com>\n"
"Language-Team: kplam <kplam@qq.com>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Generator: Poedit 1.5.4\n"
"X-Poedit-SearchPath-0: .\n"

#: contact_form.php:37 contact_form.php:1118
msgid "Contact Form Settings"
msgstr "联系表单设置"

#: contact_form.php:37
msgid "Contact Form"
msgstr "联系表单"

#: contact_form.php:167 contact_form.php:1294 contact_form.php:1353
#: contact_form.php:1746 contact_form.php:1784 contact_form.php:2033
#: contact_form.php:3113 contact_form.php:3159
msgid "Name"
msgstr "姓名"

#: contact_form.php:168 contact_form.php:1400 contact_form.php:1747
#: contact_form.php:1785 contact_form.php:2038 contact_form.php:3120
#: contact_form.php:3165
msgid "Address"
msgstr "地址"

#: contact_form.php:169 contact_form.php:1416 contact_form.php:1748
#: contact_form.php:1786
msgid "Email Address"
msgstr "电子邮箱"

#: contact_form.php:170 contact_form.php:1439 contact_form.php:1749
#: contact_form.php:1787
#, fuzzy
msgid "Phone number"
msgstr "电话："

#: contact_form.php:171 contact_form.php:1455 contact_form.php:1750
#: contact_form.php:1788 contact_form.php:2053 contact_form.php:3131
#: contact_form.php:3174
msgid "Subject"
msgstr "主题"

#: contact_form.php:172 contact_form.php:1479 contact_form.php:1751
#: contact_form.php:1789 contact_form.php:2057 contact_form.php:3136
#: contact_form.php:3178
msgid "Message"
msgstr "内容"

#: contact_form.php:173 contact_form.php:1752 contact_form.php:1790
#: contact_form.php:2062
msgid "Attachment"
msgstr "附件"

#: contact_form.php:174
msgid ""
"Supported file types: HTML, TXT, CSS, GIF, PNG, JPEG, JPG, TIFF, BMP, AI, "
"EPS, PS, CSV, RTF, PDF, DOC, DOCX, XLS, XLSX, ZIP, RAR, WAV, MP3, PPT."
msgstr ""
"支持的文件类型: HTML, TXT, CSS, GIF, PNG, JPEG, JPG, TIFF, BMP, AI, EPS, PS, "
"CSV, RTF, PDF, DOC, DOCX, XLS, XLSX, ZIP, RAR, WAV, MP3, PPT."

#: contact_form.php:175 contact_form.php:1754 contact_form.php:1792
msgid "Send me a copy"
msgstr "抄送一份给我"

#: contact_form.php:176 contact_form.php:1755 contact_form.php:1793
msgid "Submit"
msgstr "提交"

#: contact_form.php:177
msgid "Your name is required."
msgstr "姓名是必需的。"

#: contact_form.php:178
msgid "Address is required."
msgstr "地址是必需的。"

#: contact_form.php:179
msgid "A valid email address is required."
msgstr "一个有效的电子邮件地址是必需的。"

#: contact_form.php:180
msgid "Phone number is required."
msgstr "电话号码是必需的。"

#: contact_form.php:181
msgid "Subject is required."
msgstr "主题是必需的。"

#: contact_form.php:182
msgid "Message text is required."
msgstr "内容是必需的。"

#: contact_form.php:183
msgid "File format is not valid."
msgstr "文件格式是无效的。"

#: contact_form.php:184
msgid "File upload error."
msgstr "文件上传错误。"

#: contact_form.php:185
msgid "The file could not be uploaded."
msgstr "文件不能上传。"

#: contact_form.php:186
msgid "This file is too large."
msgstr "此文件太大。"

#: contact_form.php:187
msgid "Please fill out the CAPTCHA."
msgstr "请填写验证码。"

#: contact_form.php:188
msgid "Please make corrections below and try again."
msgstr "请改正，然后再试一次。"

#: contact_form.php:190
msgid "Thank you for contacting us."
msgstr "感谢您与我们联系。"

#: contact_form.php:824 contact_form.php:1076
msgid "Settings saved."
msgstr "设置已保存。"

#: contact_form.php:1021
msgid ""
"Email 'FROM' field option was changed, which may cause email messages being "
"moved to the spam folder or email delivery failures."
msgstr ""

#: contact_form.php:1031
msgid ""
"If the 'Redirect to page' option is selected then the URL field should be in "
"the following format"
msgstr "如果'重定向到页面“选项被选中，那么URL字段应为以下格式。"

#: contact_form.php:1038
#, fuzzy
msgid "Such user does not exist."
msgstr "用户不存在。设置不被保存。"

#: contact_form.php:1048
#, fuzzy
msgid ""
"Please enter a valid email address in the 'Use this email address' field."
msgstr "请在“发件人”字段输入一个有效的电子邮件地址。设置不被保存。"

#: contact_form.php:1056
#, fuzzy
msgid "Please enter a valid email address in the 'FROM' field."
msgstr "请在“发件人”字段输入一个有效的电子邮件地址。设置不被保存。"

#: contact_form.php:1078
#, fuzzy
msgid "Settings are not saved."
msgstr "设置已保存。"

#: contact_form.php:1115
msgid "All plugin settings were restored."
msgstr ""

#: contact_form.php:1120
msgid "How to Use Step-by-step Instruction"
msgstr ""

#: contact_form.php:1123 contact_form.php:3351 contact_form.php:3365
msgid "Settings"
msgstr "设置"

#: contact_form.php:1124
#, fuzzy
msgid "Additional settings"
msgstr "附加选项"

#: contact_form.php:1125
msgid "Appearance"
msgstr ""

#: contact_form.php:1126
msgid "Custom code"
msgstr ""

#: contact_form.php:1127
msgid "Go PRO"
msgstr ""

#: contact_form.php:1136
msgid "Notice"
msgstr ""

#: contact_form.php:1140
msgid "NEW_FORM"
msgstr ""

#: contact_form.php:1141
msgid ""
"If you want to create multiple contact forms, please install the Contact "
"Form Multi plugin."
msgstr ""

#: contact_form.php:1150
#, fuzzy, php-format
msgid ""
"If you would like to add a Contact Form to your page or post, please use %s "
"button"
msgstr ""
"如果你想添加联系表单到你的网站，只需复制并粘贴短码到您的文章、页面或部件："

#: contact_form.php:1156
#, php-format
msgid ""
"You can add the Contact Form to your page or post by clicking on %s button "
"in the content edit block using the Visual mode. If the button isn't "
"displayed, please use the shortcode %s or %s where * stands for Contact Form "
"language."
msgstr ""

#: contact_form.php:1165
msgid ""
"If you leave the fields empty, the messages will be sent to the email "
"address specified during registration."
msgstr "如果你留下字段为空，消息将在注册过程中被发送到指定的电子邮件地址。"

#: contact_form.php:1168
#, fuzzy
msgid "The user's email address"
msgstr "用户的电子邮件地址："

#: contact_form.php:1172
#, fuzzy
msgid "Select a username"
msgstr "新建一个用户名"

#: contact_form.php:1185
#, fuzzy
msgid ""
"Select a username of the person who should get the messages from the contact "
"form."
msgstr "请输入从联系的表单获得消息的人的名字。"

#: contact_form.php:1189
#, fuzzy
msgid "Use this email address"
msgstr "使用这个邮箱地址："

#: contact_form.php:1193
#, fuzzy
msgid "Enter the email address for receiving messages"
msgstr "输入你要转发消息的电子邮件地址。"

#: contact_form.php:1200 contact_form.php:1663 contact_form.php:1873
#: contact_form.php:1961 contact_form.php:3446
msgid "Close"
msgstr ""

#: contact_form.php:1204
#, fuzzy
msgid "Add department selectbox to the contact form"
msgstr "添加部门选择框到联系表单："

#: contact_form.php:1212 contact_form.php:1672 contact_form.php:1892
#: contact_form.php:2167
msgid "If you upgrade to Pro version all your settings will be saved."
msgstr ""

#: contact_form.php:1219 contact_form.php:1509 contact_form.php:1677
#: contact_form.php:1899 contact_form.php:2174
msgid "Unlock premium options by upgrading to Pro version"
msgstr ""

#: contact_form.php:1222 contact_form.php:1512 contact_form.php:1680
#: contact_form.php:1902 contact_form.php:2177 contact_form.php:3672
msgid "Learn More"
msgstr ""

#: contact_form.php:1230
msgid "Save emails to the database"
msgstr "将邮件保存到数据库"

#: contact_form.php:1241 contact_form.php:1255 contact_form.php:1262
msgid "Using"
msgstr ""

#: contact_form.php:1247 contact_form.php:1572 contact_form.php:1605
#: contact_form.php:1640
msgid "Please activate the appropriate option on"
msgstr ""

#: contact_form.php:1250 contact_form.php:1575 contact_form.php:1608
#: contact_form.php:1643
#, fuzzy
msgid "settings page"
msgstr "额外的设置"

#: contact_form.php:1256 contact_form.php:1581 contact_form.php:1615
#: contact_form.php:1651
#, fuzzy
msgid "Activate"
msgstr "激活captcha"

#: contact_form.php:1263 contact_form.php:1587 contact_form.php:1621
#: contact_form.php:1656
msgid "Download"
msgstr "Завантажити"

#: contact_form.php:1275
msgid "Sending method"
msgstr ""

#: contact_form.php:1280
msgid "Wp-mail"
msgstr "Wp-mail"

#: contact_form.php:1282
#, fuzzy
msgid "You can use the Wordpress wp_mail function for mailing"
msgstr "您可以使用wp_mail的邮件功能"

#: contact_form.php:1285
msgid "Mail"
msgstr "邮件"

#: contact_form.php:1287
#, fuzzy
msgid "You can use the PHP mail function for mailing"
msgstr "您可以使用wp_mail的邮件功能"

#: contact_form.php:1292
msgid "'FROM' field"
msgstr ""

#: contact_form.php:1297
msgid "User name"
msgstr "用户名"

#: contact_form.php:1299
msgid ""
"The name of the user who fills the form will be used in the field 'From'."
msgstr "填充表单的用户的名字将被显示为'发件人'"

#: contact_form.php:1304 contact_form.php:3126 contact_form.php:3170
msgid "Email"
msgstr "邮箱"

#: contact_form.php:1307
msgid "User email"
msgstr "用户的电子邮件"

#: contact_form.php:1309
msgid ""
"The email address of the user who fills the form will be used in the field "
"'From'."
msgstr "填充表单的用户的电子邮箱地址将被显示为'发件人'"

#: contact_form.php:1312
msgid ""
"If this option is changed, email messages may be moved to the spam folder or "
"email delivery failures may occur."
msgstr ""

#: contact_form.php:1318
msgid "Required symbol"
msgstr "必填符号"

#: contact_form.php:1328
msgid "Fields"
msgstr ""

#: contact_form.php:1329 contact_form.php:1356 contact_form.php:1383
#: contact_form.php:1403 contact_form.php:1442 contact_form.php:1527
msgid "Used"
msgstr ""

#: contact_form.php:1330 contact_form.php:1345 contact_form.php:1360
#: contact_form.php:1387 contact_form.php:1407 contact_form.php:1420
#: contact_form.php:1446 contact_form.php:1459 contact_form.php:1483
#, fuzzy
msgid "Required"
msgstr "必填"

#: contact_form.php:1332 contact_form.php:1365 contact_form.php:1425
#: contact_form.php:1464 contact_form.php:1488
msgid "Visible"
msgstr ""

#: contact_form.php:1333 contact_form.php:1369 contact_form.php:1429
#: contact_form.php:1468 contact_form.php:1492
msgid "Disabled for editing"
msgstr ""

#: contact_form.php:1334 contact_form.php:1393 contact_form.php:1472
#: contact_form.php:1496
msgid "Field's default value"
msgstr ""

#: contact_form.php:1341
msgid "Department selectbox"
msgstr ""

#: contact_form.php:1373
msgid "Use User's name as a default value if the user is logged in."
msgstr ""

#: contact_form.php:1374 contact_form.php:1434
msgid ""
"'Visible' and 'Disabled for editing' options will be applied only to logged-"
"in users."
msgstr ""

#: contact_form.php:1380
msgid "Location selectbox"
msgstr ""

#: contact_form.php:1433
msgid "Use User's email as a default value if the user is logged in."
msgstr ""

#: contact_form.php:1520
msgid "Attachment block"
msgstr "附件区块"

#: contact_form.php:1522
msgid "Users can attach the following file formats"
msgstr "用户可以将以下文件格式"

#: contact_form.php:1540
msgid "Add to the form"
msgstr ""

#: contact_form.php:1545
#, fuzzy
msgid "Tips below the Attachment"
msgstr "下面的附件块的提示"

#: contact_form.php:1554
#, fuzzy
msgid "'Send me a copy' block"
msgstr "显示'发送副本给我'区块"

#: contact_form.php:1667
msgid "Agreement checkbox"
msgstr ""

#: contact_form.php:1667
msgid "Required checkbox for submitting the form"
msgstr ""

#: contact_form.php:1668
msgid "Optional checkbox"
msgstr ""

#: contact_form.php:1668
msgid "Optional checkbox, the results of which will be displayed in email"
msgstr ""

#: contact_form.php:1689
msgid "Delete an attachment file from the server after the email is sent"
msgstr "发送的电子邮件后从服务器删除附件文件"

#: contact_form.php:1695
msgid "Email in HTML format sending"
msgstr ""

#: contact_form.php:1699
msgid "Display additional info in the email"
msgstr "在电子邮件中显示附加信息"

#: contact_form.php:1705
#, fuzzy
msgid "Sent from (IP address)"
msgstr "发送（IP地址）"

#: contact_form.php:1705
msgid "Example: Sent from (IP address):\t127.0.0.1"
msgstr "例如： 发送 (IP地址):\t127.0.0.1"

#: contact_form.php:1706 contact_form.php:3075 contact_form.php:3077
msgid "Date/Time"
msgstr "日期/时间"

#: contact_form.php:1706
msgid "Example: Date/Time:\tAugust 19, 2013 8:50 pm"
msgstr "例如：日期/时间:\tAugust 19, 2013 8:50 pm"

#: contact_form.php:1707 contact_form.php:3081 contact_form.php:3083
msgid "Sent from (referer)"
msgstr "发送 (请求)"

#: contact_form.php:1707
msgid ""
"Example: Sent from (referer):\thttp://bestwebsoft.com/contacts/contact-us/"
msgstr "示例：发送 (请求):\thttp://bestwebsoft.com/contacts/contact-us/"

#: contact_form.php:1708 contact_form.php:3087 contact_form.php:3089
msgid "Using (user agent)"
msgstr "使用（用户代理）"

#: contact_form.php:1708
msgid ""
"Example: Using (user agent):\tMozilla/5.0 (Windows NT 6.2; WOW64) "
"AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36"
msgstr ""
"示例：使用（用户代理）:\tMozilla/5.0 (Windows NT 6.2; WOW64) "
"AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.95 Safari/537.36"

#: contact_form.php:1713
msgid "Language settings for the field names in the form"
msgstr "表单中的字段名语言设置"

#: contact_form.php:1722
msgid "Add a language"
msgstr "添加一种语言"

#: contact_form.php:1726
msgid "Change the names of the contact form fields and error messages"
msgstr "更改联系表单字段和错误消息和名称"

#: contact_form.php:1731 contact_form.php:1827 contact_form.php:2081
#: contact_form.php:2086 contact_form.php:2096 contact_form.php:2101
#: contact_form.php:2106 contact_form.php:2111 contact_form.php:2121
#: contact_form.php:2126 contact_form.php:2135 contact_form.php:2149
#: contact_form.php:2154 contact_form.php:2159
msgid "Default"
msgstr "默认"

#: contact_form.php:1744 contact_form.php:1782
msgid "click to expand/hide the list"
msgstr ""

#: contact_form.php:1753 contact_form.php:1791
msgid "Tips below the Attachment block"
msgstr "下面的附件块的提示"

#: contact_form.php:1756 contact_form.php:1794
msgid "Error message for the Name field"
msgstr "“姓名”字段中的错误消息"

#: contact_form.php:1757 contact_form.php:1795
msgid "Error message for the Address field"
msgstr "“地址”字段的错误信息"

#: contact_form.php:1758 contact_form.php:1796
msgid "Error message for the Email field"
msgstr "“电子邮件”字段的错误消息"

#: contact_form.php:1759 contact_form.php:1797
msgid "Error message for the Phone field"
msgstr "“电话”字段的错误信息"

#: contact_form.php:1760 contact_form.php:1798
msgid "Error message for the Subject field"
msgstr "“主题”字段的错误消息"

#: contact_form.php:1761 contact_form.php:1799
msgid "Error message for the Message field"
msgstr "“消息”字段的错误消息"

#: contact_form.php:1762 contact_form.php:1800
msgid "Error message about the file type for the Attachment field"
msgstr "附件栏的文件类型错误讯息"

#: contact_form.php:1763 contact_form.php:1801
msgid ""
"Error message while uploading a file for the Attachment field to the server"
msgstr "从附件栏上传一个文件到服务器的错误信息"

#: contact_form.php:1764 contact_form.php:1802
msgid "Error message while moving the file for the Attachment field"
msgstr "从附件栏移动文件的错误信息"

#: contact_form.php:1765 contact_form.php:1803
msgid "Error message when file size limit for the Attachment field is exceeded"
msgstr "超过文件大小限制附件字段时出现错误消息"

#: contact_form.php:1766 contact_form.php:1804
msgid "Error message for the Captcha field"
msgstr "Captcha字段的错误消息"

#: contact_form.php:1767 contact_form.php:1805
msgid "Error message for the whole form"
msgstr "整个表单的错误消息"

#: contact_form.php:1770 contact_form.php:1772 contact_form.php:1808
#: contact_form.php:1810 contact_form.php:1842 contact_form.php:1844
#: contact_form.php:1856 contact_form.php:1858 contact_form.php:3514
#: contact_form.php:3516
msgid "Use shortcode"
msgstr "使用简码"

#: contact_form.php:1770 contact_form.php:1772 contact_form.php:1808
#: contact_form.php:1810 contact_form.php:1842 contact_form.php:1844
#: contact_form.php:1856 contact_form.php:1858 contact_form.php:3514
#: contact_form.php:3516
msgid "for this language"
msgstr "此语言"

#: contact_form.php:1818
#, fuzzy
msgid "Use the changed names of the contact form fields in the email"
msgstr "更改联系表单字段和错误消息和名称"

#: contact_form.php:1824
msgid "Action after email is sent"
msgstr "发送电子邮件后行动"

#: contact_form.php:1826
msgid "Display text"
msgstr "显示文本"

#: contact_form.php:1840 contact_form.php:1854
msgid "Text"
msgstr "文本"

#: contact_form.php:1865
msgid "Redirect to the page"
msgstr "重定向到的页面"

#: contact_form.php:1866
msgid "Url"
msgstr "Url"

#: contact_form.php:1877
msgid "Add field 'Reply-To' to the email header"
msgstr ""

#: contact_form.php:1879
msgid "Field 'Reply-To' will be initialized by user email"
msgstr ""

#: contact_form.php:1883
msgid "Auto Response"
msgstr ""

#: contact_form.php:1887
#, php-format
msgid ""
"You can use %%NAME%% to display data from the email field and %%MESSAGE%% to "
"display data from the Message field, as well as %%SITENAME%% to display blog "
"name."
msgstr ""

#: contact_form.php:1912 contact_form.php:2398
msgid "Save Changes"
msgstr "保存更改"

#: contact_form.php:1925
#, php-format
msgid ""
"Please enable JavaScript to change '%s', '%s' options and for fields sorting."
msgstr ""

#: contact_form.php:1925 contact_form.php:1934
msgid "Form layout"
msgstr ""

#: contact_form.php:1925 contact_form.php:1946
#, fuzzy
msgid "Submit position"
msgstr "“提交”按钮"

#: contact_form.php:1938
msgid "One column"
msgstr ""

#: contact_form.php:1941
msgid "Two columns"
msgstr ""

#: contact_form.php:1950 contact_form.php:1969 contact_form.php:1987
#: contact_form.php:2002
msgid "Left"
msgstr ""

#: contact_form.php:1953 contact_form.php:1975 contact_form.php:1990
#: contact_form.php:2008
msgid "Right"
msgstr ""

#: contact_form.php:1965
msgid "Form align"
msgstr ""

#: contact_form.php:1972 contact_form.php:2005
msgid "Center"
msgstr ""

#: contact_form.php:1980
#, fuzzy
msgid "Labels position"
msgstr "“提交”按钮"

#: contact_form.php:1984
msgid "Top"
msgstr ""

#: contact_form.php:1993
msgid "Bottom"
msgstr ""

#: contact_form.php:1998
msgid "Labels align"
msgstr ""

#: contact_form.php:2013
msgid "Errors output"
msgstr "错误输出"

#: contact_form.php:2016
msgid "Display error messages"
msgstr "显示错误消息"

#: contact_form.php:2017
msgid "Color of the input field errors."
msgstr "输入栏错误的颜色。"

#: contact_form.php:2018
msgid "Display error messages & color of the input field errors"
msgstr "显示输入栏错误的错误消息和颜色"

#: contact_form.php:2023
msgid "Add placeholder to the input blocks"
msgstr "添加占位符输入块"

#: contact_form.php:2029
msgid "Add tooltips"
msgstr "添加工具提示"

#: contact_form.php:2043
msgid "Email address"
msgstr "电子邮箱"

#: contact_form.php:2048 contact_form.php:3142 contact_form.php:3183
msgid "Phone Number"
msgstr "电话号码"

#: contact_form.php:2074
msgid "Style options"
msgstr "风格选择"

#: contact_form.php:2078
msgid "Text color"
msgstr "文字颜色"

#: contact_form.php:2083
msgid "Label text color"
msgstr "标签文字颜色"

#: contact_form.php:2088
msgid "Placeholder color"
msgstr "占位符的颜色"

#: contact_form.php:2093
msgid "Errors color"
msgstr "错误颜色"

#: contact_form.php:2098
msgid "Error text color"
msgstr "错误文字颜色"

#: contact_form.php:2103
msgid "Background color of the input field errors"
msgstr "输入栏错误的背景颜色"

#: contact_form.php:2108
msgid "Border color of the input field errors"
msgstr "输入栏错误的边框颜色"

#: contact_form.php:2113
msgid "Placeholder color of the input field errors"
msgstr "输入栏错误的占位符颜色"

#: contact_form.php:2118
msgid "Input fields"
msgstr "输入栏"

#: contact_form.php:2123
msgid "Input fields background color"
msgstr "输入栏的背景颜色"

#: contact_form.php:2128
msgid "Text fields color"
msgstr "文本字段颜色"

#: contact_form.php:2132
msgid "Border width in px, numbers only"
msgstr "边框宽度（像素，数字）"

#: contact_form.php:2137 contact_form.php:2161
msgid "Border color"
msgstr "边框颜色"

#: contact_form.php:2142
msgid "Submit button"
msgstr "“提交”按钮"

#: contact_form.php:2146
msgid "Width in px, numbers only"
msgstr "Width in px, numbers only"

#: contact_form.php:2151
msgid "Button color"
msgstr "按钮的颜色"

#: contact_form.php:2156
msgid "Button text color"
msgstr "按钮的文字颜色"

#: contact_form.php:2188
#, fuzzy
msgid "Contact Form | Preview"
msgstr "联系表单(Pro) | 预览"

#: contact_form.php:2189
msgid "Drag the necessary field to sort fields."
msgstr ""

#: contact_form.php:2385
#, fuzzy
msgid ""
"If you would like to add the Contact Form to your website, just copy and "
"paste this shortcode to your post or page or widget"
msgstr ""
"如果你想添加联系表单到你的网站，只需复制并粘贴短码到您的文章、页面或部件："

#: contact_form.php:2501
msgid "Sorry, email message could not be delivered."
msgstr "对不起，电子邮件讯息无法传递。"

#: contact_form.php:3069 contact_form.php:3071
msgid "Sent from (ip address)"
msgstr "发送（IP地址）"

#: contact_form.php:3099
msgid "Contact from"
msgstr "联系表单"

#: contact_form.php:3104 contact_form.php:3153
msgid "Site"
msgstr "站点"

#: contact_form.php:3283
msgid ""
"If you can see this MIME, it means that the MIME type is not supported by "
"your email client!"
msgstr "如果你能看到这个MIME，这意味着你的电子邮件客户端不支持该MIME类型！"

#: contact_form.php:3366
msgid "FAQ"
msgstr "常问问题"

#: contact_form.php:3367
msgid "Support"
msgstr "支持"

#: contact_form.php:3416
msgid "Are you sure that you want to delete this language data?"
msgstr "您确定要删除此语言数据吗"

#: contact_form.php:3437
msgid "Add multiple forms"
msgstr ""

#: contact_form.php:3437
msgid ""
"Install Contact Form Multi plugin to create unlimited number of contact "
"forms."
msgstr ""

#: contact_form.php:3442
msgid "Learn more"
msgstr ""

#: contact_form.php:3663
msgid "Close notice"
msgstr ""

#: contact_form.php:3668
#, fuzzy
msgid "allows to store your messages to the database."
msgstr "将邮件保存到数据库"

#: contact_form.php:3669
msgid "Manage messages that have been sent from your website."
msgstr ""

#: contact_form.php:3727
#, fuzzy
msgid "Contact form"
msgstr "联系表单"

#: contact_form.php:3740 contact_form.php:3750
#, fuzzy
msgid "Language"
msgstr "添加一种语言"

#~ msgid "Name:"
#~ msgstr "姓名："

#~ msgid "Address:"
#~ msgstr "地址："

#~ msgid "Email Address:"
#~ msgstr "电子邮箱："

#~ msgid "Phone number:"
#~ msgstr "电话："

#~ msgid "Subject:"
#~ msgstr "主题："

#~ msgid "Message:"
#~ msgstr "内容："

#~ msgid "Attachment:"
#~ msgstr "附件:"

#, fuzzy
#~ msgid ""
#~ "Please enable JavaScript to add language in the contact form, change the "
#~ "names of the contact form fields and error messages."
#~ msgstr "更改联系表单字段和错误消息和名称"

#~ msgid "What to use?"
#~ msgstr "使用什么呢？"

#, fuzzy
#~ msgid "Using Contact Form to DB by BestWebSoft"
#~ msgstr "使用联系表单到DB powered by"

#, fuzzy
#~ msgid "Activate Subscriber"
#~ msgstr "Активовані плагіни"

#, fuzzy
#~ msgid "Activate Captcha"
#~ msgstr "激活captcha"

#, fuzzy
#~ msgid "Download Captcha"
#~ msgstr "下载captcha"

#~ msgid "Phone"
#~ msgstr "电话"

#~ msgid "To send mail you can use the php mail function"
#~ msgstr "要发送邮件，可以使用PHP的邮件功能"

#~ msgid "English"
#~ msgstr "英语"

#~ msgid "or"
#~ msgstr "或"

#, fuzzy
#~ msgid ""
#~ "If you have any problems with the standard shortcode [contact_form], you "
#~ "should use the shortcode"
#~ msgstr "如果用标准短码[contact_form]遇到任何问题，你应该使用短代码"

#~ msgid "They work the same way."
#~ msgstr "他们的运作方式相同。"

#~ msgid ""
#~ "If have any problems with the standard shortcode [contact_form], you "
#~ "should use the shortcode"
#~ msgstr "如果用标准短码[contact_form]遇到任何问题，你应该使用短代码"

#~ msgid "Show with errors"
#~ msgstr "显示错误"

#~ msgid "Please enter your full name..."
#~ msgstr "请输入您的全名..."

#~ msgid "Please enter your address..."
#~ msgstr "请输入您的地址..."

#~ msgid "Please enter your email address..."
#~ msgstr "请输入你的电子邮件地址..."

#~ msgid "Please enter your phone number..."
#~ msgstr "请输入你的手机号码..."

#~ msgid "Please enter subject..."
#~ msgstr "请输入主题..."

#~ msgid "Please enter your message..."
#~ msgstr "请输入你的信息"

#~ msgid "powered by"
#~ msgstr "powered by"

#~ msgid "Activate Contact Form to DB"
#~ msgstr "激活联系表单到DB"

#~ msgid "Download Contact Form to DB"
#~ msgstr "下载联系表单到DB"

#~ msgid "Captcha"
#~ msgstr "Captcha"

#~ msgid "(powered by bestwebsoft.com)"
#~ msgstr "(powered by bestwebsoft.com)"

#~ msgid "requires"
#~ msgstr "必填"

#~ msgid ""
#~ "or higher, that is why it has been deactivated! Please upgrade WordPress "
#~ "and try again."
#~ msgstr "或更高，这就是为什么它已停用！请升级WordPress和再试一次。"

#~ msgid "Back to the WordPress"
#~ msgstr "回到 WordPress"

#~ msgid "Plugins page"
#~ msgstr "插件页面"

#, fuzzy
#~ msgid "Please, enter Your license key"
#~ msgstr "请输入你的信息"

#, fuzzy
#~ msgid "Rate the plugin"
#~ msgstr "Рекомендовані плагіни"

#, fuzzy
#~ msgid "If there is something wrong about it, please contact us"
#~ msgstr ""
#~ "Якщо у вас є запитання, звертайтесь на plugin@bestwebsoft.com або "
#~ "заповніть контактну форму на нашому сайті"

#~ msgid "Extra settings"
#~ msgstr "额外的设置"

#~ msgid "Show"
#~ msgstr "显示"

#~ msgid "Hide"
#~ msgstr "隐藏"

#~ msgid "The text in the 'From' field"
#~ msgstr "“发件人”字段中的文本"

#~ msgid "This text will be used in the 'FROM' field"
#~ msgstr "此文字将用于 'FROM' 栏"

#~ msgid "The email address in the 'From' field"
#~ msgstr "“发件人”字段中的电子邮件地址"

#~ msgid "This email address will be used in the 'From' field."
#~ msgstr "此电子邮件地址将被用于在“发件人”字段。"

#~ msgid ""
#~ "This functionality is available in the Pro version of the plugin. For "
#~ "more details, please follow the link"
#~ msgstr "此功能在Pro版本的插件可用。有关详细信息，请参考链接"

#~ msgid "Contact Form Pro"
#~ msgstr "联系表格（Pro）"

#~ msgid "Contact Form Pro Extra Settings"
#~ msgstr "联系表单（Pro）的额外设置"

#~ msgid "Contact Form Pro | Extra Settings"
#~ msgstr "Contact Form Pro | Extra Settings"

#~ msgid "Display fields"
#~ msgstr "显示字段"

#~ msgid "Display tips below the Attachment block"
#~ msgstr "显示附件区以下提示"

#~ msgid "Required fields"
#~ msgstr "必填"

#, fuzzy
#~ msgid "Display the asterisk near required fields"
#~ msgstr "Відобразити поле для телефону"

#~ msgid "You can attach the following file formats"
#~ msgstr "Користувачі можуть прикріплювати файли наступних форматів"

#, fuzzy
#~ msgid "Memory usage"
#~ msgstr "Повідомлення"

#, fuzzy
#~ msgid "Site URL"
#~ msgstr "Сайт"

#, fuzzy
#~ msgid "Please enter a valid email address."
#~ msgstr "Використовувати цю email-адресу:"

#~ msgid "Installed plugins"
#~ msgstr "Встановлені плагіни"

#~ msgid "Recommended plugins"
#~ msgstr "Рекомендовані плагіни"

#~ msgid "Install %s"
#~ msgstr "Встановлено %s"

#~ msgid "Install now from wordpress.org"
#~ msgstr "Встановити з wordpress.org"

#, fuzzy
#~ msgid "Active Plugins"
#~ msgstr "Активовані плагіни"

#, fuzzy
#~ msgid "Inactive Plugins"
#~ msgstr "Активовані плагіни"

#, fuzzy
#~ msgid "Send to support"
#~ msgstr "Підтримка"

#~ msgid "Contact Form Options"
#~ msgstr "Налаштування контактної форми"

#~ msgid "Display Attachment tips"
#~ msgstr "Показувати підказки для прикріплення файлів"

#~ msgid "Please enter a valid email address. Settings are not saved."
#~ msgstr "Будь ласка, введіть правильну ел.адресу. Налаштування не збережено."

#~ msgid "E-Mail Address"
#~ msgstr "E-Mail адреса"

#~ msgid "E-Mail Addresse:"
#~ msgstr "E-mail адрес:"

#~ msgid "Install Now"
#~ msgstr "Установить сейчас"
